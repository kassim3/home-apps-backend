<?php

namespace App\Http\Controllers\ShoppingItem;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShoppingItem;

class ShoppingItemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request, $id=null) {

        if(!$id) return $this->jsendSuccess(ShoppingItem::all());
        
        $shoppingItem = ShoppingItem::find($id);

        if(!$shoppingItem) return $this->jsendFail('Shopping item not found', 404);
        
        return $this->jsendSuccess($shoppingItem);        
    }

    public function store(Request $request) {
        
        $this->validate($request, [
            'name' => 'required',
            'quantity' => 'nullable|integer',
            'price' => 'nullable|numeric',
            'bought' => 'boolean',
        ]);
            
        $shoppingItem = new ShoppingItem;
        
        $shoppingItem->name = $request->input('name');
        if ($request->filled('notes')) $shoppingItem->notes = $request->input('notes');
        if ($request->filled('price')) $shoppingItem->price = $request->input('price');
        if ($request->filled('quantity')) $shoppingItem->quantity = $request->input('quantity');
        if ($request->filled('bought')) $shoppingItem->bought = $request->input('bought');
        if ($request->filled('shop')) $shoppingItem->shop = $request->input('shop');
            
        $user = $request->user();
        $shoppingItem->user_id = $user->id;
        $shoppingItem->last_updated_by = $user->id;

        $shoppingItem->save();

        return $this->jsendSuccess($shoppingItem);

    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'quantity' => 'nullable|integer',
            'price' => 'nullable|numeric',
            'bought' => 'nullable|boolean',
        ]);
            
        $shoppingItem = ShoppingItem::find($id);

        if(!$shoppingItem) return $this->jsendFail('Shopping item not found', 404);
        
        if ($request->filled('name')) $shoppingItem->name = $request->input('name');
        if ($request->filled('notes')) $shoppingItem->notes = $request->input('notes');
        if ($request->filled('price')) $shoppingItem->price = $request->input('price');
        if ($request->filled('quantity')) $shoppingItem->quantity = $request->input('quantity');
        if ($request->filled('bought')) $shoppingItem->bought = $request->input('bought');
        if ($request->filled('shop')) $shoppingItem->shop = $request->input('shop');
            
        $user = $request->user();
        $shoppingItem->last_updated_by = $user->id;

        $shoppingItem->save();

        return $this->jsendSuccess($shoppingItem);
    }

    public function delete(Request $request, $id) {
            
        $shoppingItem = ShoppingItem::find($id);

        if(!$shoppingItem) return $this->jsendFail('Shopping item not found', 404);

        $shoppingItem->delete();

        return $this->jsendSuccess();
    }
}
