<?php

namespace App\Http\Controllers\Todo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Todo;
use App\Models\TodoGroup;

class TodoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request, $id=null) {

        if(!$id) {
            $list = Todo::with('group')->orderBy('due_date')->get();
            return $this->jsendSuccess($list);
        }
        
        $todo = Todo::with('group')->find($id);

        if(!$todo) return $this->jsendFail('Item not found', 404);
        
        return $this->jsendSuccess($todo);        
    }

    public function store(Request $request) {
        
        $this->validate($request, [
            'name' => 'required'
        ]);
            
        $todo = new Todo;
        
        $todo->name = $request->input('name');
        if ($request->filled('due_date')) {
            $due_date = new \DateTime();
            $due_date->setTimestamp($request->input('due_date'));
            $todo->due_date = $due_date;
        }
        if ($request->filled('complete')) $todo->complete = $request->input('complete');
        
        if ($request->filled('group_id')) $group = TodoGroup::find($request->input('group_id'));
        else {
            $group = new TodoGroup;
            $group = $group->default();
        }
        
        if(!$group) return $this->jsendFail('Group not found', 400);

        $todo->group_id = $group->id;

        $user = $request->user();
        $todo->user_id = $user->id;
        $todo->last_updated_by = $user->id;
        
        $todo->save();

        return $this->jsendSuccess($todo);
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'complete' => 'boolean',
        ]);
            
        $todo = Todo::find($id);

        if(!$todo) return $this->jsendFail('Item not found', 404);
        
        if ($request->filled('name')) $todo->name = $request->input('name');
        if ($request->filled('due_date')) {
            $due_date = new \DateTime();
            $due_date->setTimestamp($request->input('due_date'));
            $todo->due_date = $due_date;
        }
        
        if ($request->filled('complete')) $todo->complete = $request->input('complete');
        
        if ($request->filled('group_id')) $group = TodoGroup::find($request->input('group_id'));
        else $group = TodoGroup::find($todo->group_id);
        
        if(!$group) return $this->jsendFail('Group not found', 400);

        $todo->group_id = $group->id;

        $user = $request->user();
        $todo->last_updated_by = $user->id;

        $todo->save();

        return $this->jsendSuccess($todo);
    }

    public function delete(Request $request, $id) {
            
        $todo = Todo::find($id);

        if(!$todo) return $this->jsendFail('Item not found', 404);

        $todo->delete();

        return $this->jsendSuccess();
    }
}
