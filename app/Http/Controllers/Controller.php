<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function jsendSuccess($data = null, $code = 200) {
        return response()->json([
            'status' => 'success',
            'data' => $data
        ], $code);
    }

    public function jsendFail($data = null, $code = 400) {
        return response()->json([
            'status' => 'fail',
            'data' => $data
        ], $code);
    }

    public function jsendError($message = 'An error occurred', $code = 500) {
        return response()->json([
            'status' => 'error',
            'message' => $message
        ], $code);
    }
}
