<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TodoGroup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function todos() {
        return $this->hasMany('App\Models\Todo');
    }

    public function default() {
        return $this->where('name', 'default')->first();
    }
}
