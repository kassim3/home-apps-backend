<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_items', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->unsignedInteger('last_updated_by');
            $table->foreign('last_updated_by')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->string('name');
            $table->string('notes')->nullable();
            $table->decimal('price',  8, 2)->nullable();
            $table->unsignedInteger('quantity')->nullable();
            $table->boolean('bought')->default(false);
            $table->enum('shop', ['tesco', 'pak'])->default('tesco');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_lists');
    }
}
