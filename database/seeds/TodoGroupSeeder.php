<?php

use Illuminate\Database\Seeder;

class TodoGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todo_groups')->insert([
            'name' => 'default',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
    }
}
