<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });

$router->group(['prefix' => 'auth'], function() use ($router)
{
    $router->post('register', 'AuthController@register');
    $router->post('login', 'AuthController@login');
});

$router->group(['middleware' => 'auth'], function() use ($router) {

    $router->group(['prefix' => 'shoppingItems', 'namespace' => 'ShoppingItem'], function() use ($router)
    {
        $router->get('', 'ShoppingItemController@index');
        $router->get('{id}', 'ShoppingItemController@index');
        $router->post('', 'ShoppingItemController@store');
        $router->patch('{id}', 'ShoppingItemController@update');
        $router->delete('{id}', 'ShoppingItemController@delete');
    });

    $router->group(['prefix' => 'todolist', 'namespace' => 'Todo'], function() use ($router)
    {
        $router->get('', 'TodoController@index');
        $router->get('{id}', 'TodoController@index');
        $router->post('', 'TodoController@store');
        $router->patch('{id}', 'TodoController@update');
        $router->delete('{id}', 'TodoController@delete');
    });
});
