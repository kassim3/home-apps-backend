One more thing we should do before any deployment is to manually copy our application storage folder to the /var/www/app directory on the server for the first time. You might want to create another Envoy task to do that for you. We also create the .env file in the same path to set up our production environment variables for Laravel. These are persistent data and will be shared to every new release.

Nginx config

```
server {
         listen 80;
         listen [::]:80 ipv6only=on;

         # Log files for Debugging
         access_log /var/log/nginx/laravel-access.log;
         error_log /var/log/nginx/laravel-error.log;

         # Webroot Directory for Laravel project
         root /home/kassim/home-apps-backend/current/public;
         index index.php index.html index.htm;

         # Your Domain Name
         server_name home-apps-api-beta.kakageek.com;

         location / {
                 try_files $uri $uri/ /index.php?$query_string;
         }

         # PHP-FPM Configuration Nginx
         location ~ \.php$ {
                 try_files $uri =404;
                 fastcgi_split_path_info ^(.+\.php)(/.+)$;
                 fastcgi_pass unix:/run/php/php7.2-fpm.sock;
                 fastcgi_index index.php;
                 fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                 include fastcgi_params;
         }
 }
 ```