@servers(['web' => ['kassim@home-apps-beta.kakageek.com']])

@setup
    $repository = 'git@bitbucket.org:kassim3/home-apps-backend.git';
    $app_dir = '/home/kassim/home-apps-backend';
    $releases_dir = "$app_dir/releases";
    $current_dir = "$app_dir/current";
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;

    $env = isset($env) ? $env : "production";
@endsetup

@story('deploy')
    clone_repository
    run_composer
    update_symlinks
    run_migrate
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir -p {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_migrate')
echo 'Starting migrations'
cd {{ $new_release_dir }}
php artisan migrate --env={{ $env }} --force --no-interaction
@endtask

@task('update_symlinks')
echo "Linking storage directory"
rm -rf {{ $new_release_dir }}/storage
ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

echo 'Linking .env file'
ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

echo 'Linking current release'
ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask

@task('run_composer')
    echo "Running composer install"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts --no-dev -o
@endtask

@task('rollback', ['on' => 'web'])
    echo 'Rolling back'
    cd {{ $releases_dir }}
    ln -nfs {{ $releases_dir }}/$(find . -maxdepth 1 -name "20*" | sort  | tail -n 2 | head -n1) {{ $current_dir }}
    echo "Rolled back to: {{ $releases_dir }}/$(find . -maxdepth 1 -name "20*" | sort  | tail -n 2 | head -n1)"
@endtask